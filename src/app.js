import express from "express";
import morgan from "morgan";
import cors from "cors";
import config from "./config";

import UserRouter from "./routes/user.routes";
import PostRouter from "./routes/post.routes";
import CommentRouter from "./routes/comment.routes";
import CartRouter from "./routes/cart.routes";

const initDB = require("./database/mongodtbase");
initDB();

const app = express();

app.get("/", (req, res) => {
  res.send("hola,  probando desde aqui 🫡");
});

app.use(cors());
app.set("port", config.port);

app.use(express.json());
app.use(morgan("dev"));
app.use(express.static("storage"));

app.use((req, res, next) => {
  console.log("Time:", new Date());
  next();
});

app.use("/api/user", UserRouter);
app.use("/api/post", PostRouter);
app.use("/api/comment", CommentRouter);
app.use("/api/cart", CartRouter);

export default app;
