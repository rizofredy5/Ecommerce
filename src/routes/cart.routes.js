import { Router } from "express";
import { validToken } from "../libs/validToken";
import {
  addProductCart,
  deleteProductCart,
  editProductCart,
  getCart,
} from "../controllers/cart.controller";
const router = Router();

router.post("/add/products-cart", validToken, addProductCart);

router.put("/products-cart/:productId", validToken, editProductCart);

router.delete("/products-cart/:productId", validToken, deleteProductCart);

router.get("/product-cart", validToken, getCart);

export default router; 

//? Peticion POST ADDPRODUCTCART
//TODO -> "https://localhost:2320/api/cart/add/products-cart"
//* En los headers enviamos el token del usuario que esta agregando el  producto al  carrito
//* En el body tomamos los datos del producto que queremos en el carrito y los almacenamos

//? Peticion PUT EDITPRODUCTCART
//TODO -> "https://localhost/2320/api/cart/product-cart/:productId"
//*En los headers enviamos el token del usuario 
//! En el carrito enviamos "add" si queremos aumentar la cantidad del producto, o "del" si queremos disminuir la cantidad del producto

//?  Peticion DELETE DELETEPRODUCTCART
//TODO -> "https://localhost/2320/api/cart/products-cart/:productId"
//*En los headers enviamos  el token del usuario
//! Em el parametro enviamos  el productId  del producto que eliminaremos del carrito

//? Peticion GET GETCART
//TODO -> "https://localhost:2320/api/cart/product-cart"
//* En los headers el token del usuario que esta  viendo su carrito