import { Router } from "express";
import { login, register } from "../controllers/auth.controller";
import { upload } from "../libs/upload";  //? Le doy el formato a las imgs que se almacenan en el storage
import {
  deleted,
  editPhoto,
  getAll,
  update,
} from "../controllers/user.controller";
import { validToken } from "../libs/validToken"; 
import { getPaginatedata } from "../libs/pagination"
const router = Router();

router.post(
  "/register", register
);

router.post("/login", login);

router.put("/:id/edit", validToken, update);

router.put(
  "/editPhoto/:id",
  validToken,
  upload.fields([{ name: "photoProfile", maxCount: 1 }]),
  editPhoto
);

router.delete("/:id/delete", validToken, deleted);

router.get("/getAll/:pag?/:perpage?", getPaginatedata, getAll);


export default router;

//? Peticion POST REGISTER
//TODO -> "http://localhost:2320/api/user/register"
//*En el body enviamos (name, email, password) 

//? Peticion POST LOGIN
//TODO -> "http://localhost:2320/api/user/login"
//*En el body solo enviamos(name,email)

//? Peticion PUT UPDATE
//TODO -> "http://localhost:2320/api/user/:id/edit"
//!En el parametro  se envia  el id del usuario en sesion
// !En los header enviamos el token del usuario como ['valid-token']
//*En el body se envian los campos que queremos actualizar y su nuevo valor

//? Peticion PUT EDITPHOTO
//TODO -> "http://localhost:2320/api/user/editPhoto/:id" 
//!En el parametro  se envia el id del usuario en sesion
// !En los header enviamos el token como ['valid-token']
//*En el body se envia el dato como ( 'photoProfile'), como un "form-data"

//? Peticion DELETE DELETED
//TODO -> "http://localhost:2320/api/user/:id/delete"
//!En el parametro se envia el id del usuario en sesio
//! En los headers enviamos el token del usuario como ['valid-token']

//? Peticion GET GETALL
//TODO -> "http://localhost:2320/api/user/getAll/:pag?/:perpage?"
//*En en parametro enviamos el formato de la paginacion
//*No enviamos nada en el body