import { Router } from "express";
import { validToken } from "../libs/validToken";
import {
  GetPaginateComment,
  PostComment,
  deletedComment,
  rating,
} from "../controllers/comment.controller";
import { getPaginatedata } from "../libs/pagination";
const router = Router();

router.post("/:id/rating", validToken, rating);

router.post("/:public_id/addComment", validToken, PostComment);

router.get("/:confirmPubli/:pag?/:perpage?", getPaginatedata, GetPaginateComment);

router.delete("/:id/deleted/comment", validToken, deletedComment);

export default router;

//? Peticion POST RATING
//TODO -> "https://localhost:2320/api/post/:id/rating"
//! En el parametro enviar el id de la publicacion a la que vamos a dar la estrellas
//*En los headers enviamos el token del usuario que esta haciendo el rating 

//?Peticion POST POSTCOMMENT
//TODO -> "https://localhost:2320/api/post/:public_id/addComment"
//! En el parametro enviamosel public_id de  la publicacion que vamos a comentar
//*En los headers enviamos el token del  usuario que esta comentando
//*En el body enviamos el [comment,(confirmPubli => se envia en el body el id de la publicacion)]

//? Peticion GET GETPAGINATECOMMENT
//TODO -> "https://localhost:2320/api/post/:confirmPubli/:pag?/:perpage?"
//!En el parametro enviamos el confirmPubli  para saber que publicacion fue comentada, tambien enviamos la paginacion

//? Peticion DELETE DELETEDCOMMENT
//TODO -> "https://localhost:2320/api/post/:id/deleted/comment"
//! En el parametro enviamos el id del comentario que se va a eliminar
//* En  los headers enviamos el token del usuario que hizo el comentario para eliminarlo
