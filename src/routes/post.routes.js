import { getPaginatedata } from "../libs/pagination";
import { Router } from "express";
import { upload } from "../libs/upload";
import { validToken } from "../libs/validToken";
import {
  GetAllPost,
  GetPostId,
  GetSearch,
  agregate,
  deletedPost,
  editPost,
  getPropPost,
  pagination,
  random,
} from "../controllers/post.controller";
const router = Router();

router.post("/agregate", validToken, upload.array("Photos", 5), agregate);

router.put("/:id/edited/post", validToken, editPost);

router.delete("/:id/deleted/post", validToken, deletedPost);

router.get(
  "/:userId/post/:pag?/:perpage?",
  validToken,
  getPaginatedata,
  getPropPost
);

router.get("/:id/postId", GetPostId);

router.get("/search/:query/:pag?/:perpage?", getPaginatedata, GetSearch);

router.get("/:pag?/:perpage?", getPaginatedata, GetAllPost);

export default router;

//? Peticion POST AGREGATE
//TODO -> "http://localhost:2320/api/post/agregate"
//*Enviamos en los headers el token  del usuario  quien hizo la publicacion
//*!Las  imagenes se envian como ["Photos"], el limite  es 5 archivos
//*En el body enviamos los  campos que vamos a almacenar como form-data [Photos,name, desc,category,price,(userId => el id del usuario que hace el post)]

//? Peticion PUT EDITPOST
//TODO   ->  "http://localhost:2320/api/post/:id/edited/post"
//*! En el parametro enviamos el id del POST que vamos a editar
//*Enviamos en los headers el token  del usuario  quien hizo la publicacion
//*En el body enviamos los  campos que vamos a editar [titulo, desc, category, price ]

//? Peticion DELETE DELETEDPOST
//TODO ->  "http://localhost:2320/api/post/:id/deleted/post"
//!En el parametro enviamos el id del POST que queremos eliminar
//*Enviamos en los headers el token  del usuario  quien hizo la publicacion

//? Peticion GET GETPROPPOST
//TODO -> "http://localhost:2320/api/post/:userId/:pag?/:perpage?"
//*En el paremetro enviamos  el userId del usuario y la paginacion para mostrar las publicaciones porpias paginadas
//*Enviamos en los headers el token  del usuario  quien hizo la publicacion

//? Peticion  GET GETPOSTID
//TODO -> "http://localhost:2320/api/post/:id/postId"
//*En el paremetro enviamos el id de la publicacion que queremos ver

//?  Peticion GET GETSEARCH
//TODO -> "http://localhost:2320/api/post/search/:query"
//*Enviamos en el parametro  el query que queramos buscar (titulo o category)

//? Peticion GET GETALL
//TODO -> "http://localhost:2320/api/post/:pag?/:perpage?"
//*Enviamos la paginacion en el parametro
