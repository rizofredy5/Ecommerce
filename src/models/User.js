import mongoose from "mongoose";
import bcrypt from "bcrypt";

const UserSchema = mongoose.Schema({
  name: {
    type: String,
  },
  email: {
    type: String,
    uniqued: true,
  },
  password: {
    type: String,
  },
  photoProfile: {
    type: String,
    default: "",
  },
  telefono:{
    type: String,
    default: ""
  },
  city:{
    type: String,
    default: ""
  },
  from:{
    type: String,
    default: ""    
  },
  isAdmin: {
    type: String,
    default: false,
  },
});

UserSchema.statics.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(5);
  return await bcrypt.hash(password, salt);
};

UserSchema.statics.comparePassword = async (password, receivedPassword) => {
  return await bcrypt.compare(password, receivedPassword);
};

module.exports = mongoose.model("User", UserSchema);
