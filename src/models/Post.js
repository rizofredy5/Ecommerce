import mongoose from "mongoose";

const PostSchema = mongoose.Schema({
  titulo: {
    type: String,
    uniqued: true,
  },
  desc: {
    type: String,
  },
  category: {
    type: Array,
  },
  user: {
    _id: String,
    name: String,
    photoProfile: String,
  },
  price: {
    tpye: String,
  },
  photos: [{ base64sm: String, base64md: String, base64lg: String}],
  userId: {
    type: String,
  },
  inCart: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("Post", PostSchema);
