import mongoose from "mongoose";

const CartSchema = mongoose.Schema({
  titulo: {
    type: String,
    uniqued: true,
  },
  photos: {
    type: String,
  },
  amount: {
    type: Number,
  },
  price: {
    type: Number,
  },
  user: {
    _id: String,
    name: String,
    photoProfile: String,
  }
});

module.exports = mongoose.model("Carts", CartSchema);
