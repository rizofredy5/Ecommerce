import mongoose from "mongoose";

const CommentSchema = mongoose.Schema({
  stars: {
    type: Number,
    min: 1,
    max: 5,
  },
  comment: {
    type: String,
  },
  public_id: { 
    type: String 
  },
  confirmPubli: { 
    type: String 
  },
  user: {
    _id: String,
    name: String,
    photoProfile: String,
    isAdmin: String,
  },
});

module.exports = mongoose.model("Comentario", CommentSchema);
