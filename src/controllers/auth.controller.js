import User from "../models/User"; //? Modelo  USER
import config from "../config";  //? Informacion variables de entorno
import jwt from "jsonwebtoken"; //? Token

export const register = async (req, res) => {
  try {
    const { name, email, password } = req.body;
    if (
      name === undefined || 
      email === undefined || 
      password === undefined
    ) {
      return res.status(203).json({
        message: "Completa todos los campos para continuar",
        status: false,
      });
    }
    var existEmail = await User.findOne({ email }); //TODO Valida si el datos ya se encuentra almacenado
    if (existEmail) {
      return res
        .status(204)
        .json({ message: "Este correo ya existe", status: false });
    }

    var existName = await User.findOne({ name }); //TODO Valida si el datos ya se encuentra almacenado
    if (existName) {
      return res
        .status(204)
        .json({ message: "Nombre de usuario existente", status: false });
    }

    const pass = await User.encryptPassword(password); //TODO hashed del password
    const newUser = new User({
      name,
      email,
      password: pass,
    });

    const user = await newUser.save();
    res
      .status(200)
      .json({ message: "Registrado exitosamente", status: true, user });
  } catch (err) {
    res.status(500).json(err);
  }
};

export const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    if (
      email === undefined || 
      password === undefined
    ) {
      return res
        .status(203)
        .json({
          message: "Ingresa todos los datos  para  iniciar sesión",
          status: false,
        });
    }

    var existUser = await User.findOne({ email }); //TODO Valida si el correo ya esta almacenado en la base de datos
    if (!existUser) {
      return res
        .status(203)
        .json({ message: "Email existente", status: false });
    }

    const validContra = await User.comparePassword( //TODO Compara la password del usuario al loguearse
      password,
      existUser.password
    );
    if (!validContra) { //TODO Si la password no coincide no continua
      return res
        .status(203)
        .json({ message: "Contraseña  incorrecta", status: true });
    }

    const token = jwt.sign(
      {
        id: existUser.id,
        name: existUser.name,
        email: existUser.email,
        photoProfile: existUser.photoProfile,
        city: existUser.city,
        from: existUser.from,
        isAdmin: existUser.isAdmin,
      },
      config.SECRET,
      { expiresIn: "365d" }
    );

    const newUser = {
      id: existUser.id,
      token_temp: token,
    };

    const resp = await User.updateOne({ _id: existUser.id }, newUser);
    if (!resp) {
      return res.status(203).json({
        message: "Error al iniciar sesión",
        status: false,
        token,
        usuario: {
          id: existUser.id,
          name: existUser.name,
          email: existUser.email,
          photoProfile: existUser.photoProfile,
          city: existUser.city,
          from: existUser.from,
          isAdmin: existUser.isAdmin,
        },
      });
    }

    return res.status(200).json({
      message: "Bienvenido",
      status: true,
      token,
      usuario: {
        id: existUser.id,
        name: existUser.name,
        email: existUser.email,
        photoProfile: existUser.photoProfile,
        city: existUser.city,
        from: existUser.from,
        isAdmin: existUser.isAdmin,
      },
    });
  } catch (err) {
    res.status(500).json(err);
  }
};
