import Comment from "../models/Comment";
import User from "../models/User";

export const rating = async (req, res) => {
  try {
    const { id } = req.params; //TODO -> el id de la  publicacion a la que se va a reaccionar
    const { stars, comment } = req.body;

    if (
      id === undefined
    ) {
      return res
        .status(204)
        .json({ message: "No hay parametros👈", status: false });
    }

    const { _id, name, photoProfile } = req.user;  //! Datos del usuario por medio del token que se almacenan
    const newRating = Comment({
      stars,
      comment,
      user: { _id, name, photoProfile },
    });

    newRating
      .save()
      .then((data) =>
        res.json({ message: "Rating agregado", status: true, newRating, data })
      )
      .catch((error) =>
        res.json({ message: "Error  de  rating", status: false, error })
      );
    return;
  } catch (err) {
    res.status(500).json(err);
  }
};

export const PostComment = async (req, res) => {
  try {
    const { public_id } = req.params; //TODO -> el public_id de la publicacion que se va a comentar
    const { comment, confirmPubli } = req.body;

    if (
      public_id === undefined ||
      comment === undefined ||
      confirmPubli === undefined
    )
      return res.status(203).json({
        message: "Si quieres comentar completa los campos",
        status: false,
      });

    const { _id, name, photoProfile, isAdmin } = req.user; //! Datos del usuario por medio del token que se almacenan
    const newComment = Comment({
      comment,
      confirmPubli,
      user: { _id, name, photoProfile, isAdmin },
    });

    newComment
      .save()
      .then((data) =>
        res.json({
          message: "Comentario agregado correctamente",
          status: true,
          newComment,
          data,
        })
      )
      .catch((error) =>
        res.json({
          message: "Error al comentar publicacion, intentalo nuevamente",
          status: false,
          error,
        })
      );
    return;
  } catch (err) {
    res.status(500).json(err);
  }
};

export const GetPaginateComment = async (req, res) => {
  try {
    const cant = await Comment.count();
    Comment.find({confirmPubli})
      .skip(req.body.skippag)
      .limit(req.body.limit)
      .sort({ _id: -1 })
      .then(async (data) => {
        for (let index = 0; index < data.length; index++) {
          const element = data[index];

          const usertemp = await User.findById(element.user._id);

          if (!usertemp) {
            data[index].user = {
              _id: 0,
              name: "indefinido",
              photoProfile: "",
            };
          } else {
            data[index].user = {
              _id: usertemp._id,
              name: usertemp.name,
              photoProfile: usertemp.photoProfile,
            };
          }
        }

        res.json({
          message: "Mostrando comment",
          status: true,
          data,
          pagination: {
            pag: req.params.pag,
            perpage: req.body.limit,
            pags: Math.ceil(cant / req.body.limit),
          },
        });
      })
      .catch((error) => {
        console.log(error.message);
        res.json({
          message: "Error al cargar todas comment",
          status: false,
          error,
        });
      });
  } catch (e) {
    res.send(e.message);
  }
};

export const deletedComment = async (req, res) => {
  if ( req.user.id === req.params.id  || req.user.isAdmin == true) {
    try {
      const { id } = req.params;
      Comment.deleteOne({ _id: id })
        .then((data) =>
          res.json({
            message: "Acabas de eliminar un  comentario",
            status: true,
            Comment,
            data,
          })
        )
        .catch((error) =>
          res.json({
            message: "Error al intentar eliminar el comentario",
            status: false,
            error,
          })
        );
      return;
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(403).json("No estar permitido");
  }
};
