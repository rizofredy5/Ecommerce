import Post from "../models/Post"; //? Modelo POST
import User from "../models/User"; //? Modelo USER
import { procesamientoPublicacion } from "../libs/procesamiento"; //! Base64 de  imagenes

export const agregate = async (req, res) => {
  try {
    const { titulo, desc, category, price, userId } = req.body;

    if (
      titulo === undefined ||
      desc === undefined ||
      category === undefined ||
      price === undefined ||
      userId === undefined
    ) {
      res.status(200).json({
        message: "Completa  todos los campos para continuar",
        status: false,
      });
      return;
    }

    const photos = await procesamientoPublicacion(req.files); //TODO procsamiento de base64 de las  imagenes

    const { _id, name, photoProfile } = req.user; //? Dato del token del usuario
    const newPost = Post({
      titulo,
      desc,
      category,
      price,
      photos,
      userId,
      user: { _id, name, photoProfile },
    });

    newPost
      .save()
      .then((data) =>
        res.json({
          message: "Post cargado correctamente",
          status: true,
          data,
        })
      )
      .catch((error) =>
        res.json({ message: "Error al cargar post", status: false, error })
      );
    return;
  } catch (err) {
    res.status(500).json(err);
  }
};

export const editPost = async (req, res) => {
  if (req.user.id || req.user.isAdmin == true) {
    //! Validacion para admin y user para editar la publicacion
    try {
      const { id } = req.params; //TODO Id de la publicacion que se va a editar
      const { titulo, desc, category, price } = req.body;

      if (id === undefined)
        return (
          res.status(203),
          json({ message: "Sin enviar parametro👈", status: false })
        );

      const edited = Post({
        titulo,
        desc,
        category,
        price,
      });

      Post.updateOne(
        { _id: id },
        {
          $set: {
            titulo,
            desc,
            category,
            price,
          },
        }
      )

        .then((data) =>
          res.json({
            message: "Actualizado correctamente",
            status: true,
            edited,
            data,
          })
        )
        .catch((error) =>
          res.json({
            message: "Error al intentar actualizar...",
            status: false,
            error,
          })
        );
      return;
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(403).json("No estás permitido");
  }
};

export const deletedPost = async (req, res) => {
  if (req.user.id || req.user.isAdmin == true) {
    //! Solo el admin y el usuario que la hizo pueden borrarla
    try {
      const { id } = req.params; //TODO Id de la publicacion que se desea eliminar
      Post.deleteOne({ _id: id })
        .then((data) =>
          res.json({
            message: "Acabas de eliminar este post",
            status: true,
            Post,
            data,
          })
        )
        .catch((error) =>
          res.json({
            message: "Error al eliminar post, intentalo nuevamente",
            status: false,
            error,
          })
        );
      return;
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(403).json("No estas permitido");
  }
};

export const getPropPost = async (req, res) => {
  try {
    const { userId } = req.params; //  TODO -> Userid que se almacena en el post para listar los datos  propios
    const cant = await Post.count();
    Post.find({ userId })
      .skip(req.body.skippag)
      .limit(req.body.limit)
      .sort({ _id: -1 })
      .then(async (data) => {
        res.json({
          message: "Mostrando publicaciones",
          status: true,
          data,
          pagination: {
            pag: req.params.pag,
            perpage: req.body.limit,
            pags: Math.ceil(cant / req.body.limit),
          },
        });
      })
      .catch((error) => {
        console.log(error.message);
        res.json({
          message: "Error al cargar todas publicaciones",
          status: false,
          error,
        });
      });
  } catch (e) {
    res.send(e.message);
  }
};

export const GetPostId = async (req, res) => {
  try {
    const { id } = req.params; //TODO -> id en el parametro  de la publicacion que se va a ver

    Post.findById(id)
      .then((data) =>
        res.json({
          message: "Publicacion encontrada",
          status: true,
          Post,
          data,
        })
      )
      .catch((error) =>
        res.json({
          message:
            "Publicacion no encontrada, es posible que se haya sido eliminada",
          status: false,
          error,
        })
      );
    return;
  } catch (error) {
    res.json({ message: "Error catch,publicacion no encontrada" });
    return;
  }
};

export const GetSearch = async (req, res) => {
  try {
    const cant = await Post.count();
    const Search = await Post.find({
      category: new RegExp(req.params.query, "i"),
      titulo: new RegExp(req.params.query, "i"),
    })

      .skip(req.body.skippag)
      .limit(req.body.limit)
      .sort({ _id: -1 })
      .then(async (data) => {
        res.json({
          message: "Mostrando publicaciones",
          status: true,
          data,
          pagination: {
            pag: req.params.pag,
            perpage: req.body.limit,
            pags: Math.ceil(cant / req.body.limit),
          },
        });
      })
      .catch((error) => {
        console.log(error.message);
        res.json({
          message: "Error al cargar todas publicaciones",
          status: false,
          error,
        });
      });
  } catch (err) {
    res.status(500).json(err);
  }
};

export const GetAllPost = async (req, res) => {
  try {
    const cant = await Post.count();
    Post.find()
      .skip(req.body.skippag)
      .limit(req.body.limit)
      .sort({ _id: -1 })
      .then(async (data) => {
        for (let index = 0; index < data.length; index++) {
          const element = data[index];

          const usertemp = await User.findById(element.user._id);

          if (!usertemp) {
            data[index].user = {
              _id: 0,
              name: "indefinido",
              profile_picture: "",
            };
          } else {
            data[index].user = {
              _id: usertemp._id,
              name: usertemp.name,
              profile_picture: usertemp.profile_picture,
            };
          }
        }

        res.json({
          message: "Mostrando publicaciones",
          status: true,
          data,
          pagination: {
            pag: req.params.pag,
            perpage: req.body.limit,
            pags: Math.ceil(cant / req.body.limit),
          },
        });
      })
      .catch((error) => {
        console.log(error.message);
        res.json({
          message: "Error al cargar todas publicaciones",
          status: false,
          error,
        });
      });
  } catch (e) {
    res.send(e.message);
  }
};
