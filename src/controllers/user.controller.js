import User from "../models/User"; //? Modelo USER
import { procesamientounidad } from "../libs/procesamiento"; //! Base64 de imagenes

export const update = async (req, res) => {
  if (req.user.id || req.user.isAdmin == true) { //! Validacion de quien puede editar la cuenta
    try {
      const { id } = req.params;
      const { name, city, from, telefono } = req.body;

      if (id === undefined)
        return res
          .status(203)
          .json({ message: "Sin enviar parametro👈", status: false });

      const edit = User({
        name,
        city,
        from,
        telefono,
      });

      User.updateOne(
        { _id: id },
        {
          $set: {
            name,
            from,
            city,
            telefono,
          },
        }
      )

        .then((data) =>
          res.json({ message: "Perfil actualizado", status: true, edit, data })
        )
        .catch((error) =>
          res.json({
            message: "Error al actualizar perfil",
            status: false,
            error,
          })
        );
      return;
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    return res.status(403).json("No tienes permitido hacer esta funcion");
  }
};

export const deleted = async (req, res) => {
  if (req.user.id === req.params.id || req.user.isAdmin == true) { //! Validacion de quien puede borrar la cuenta
    try {
      const deleUser = await User.findByIdAndDelete(req.params.id); //TODO Busca el dato que se va a eliminar por el id
      res.status(200).json({
        message: "Su usuario ha sido eliminado correctamente",
        deleUser,
      });
    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(403).json("Solo puede borrar tu cuenta");
  }
};

export const getAll = async (req, res) => {
  try {
    const cant = await User.count();
    User.find(
      {},
      {
        name: 1,
        email: 1,
        photoProfile: 1,
        telefono: 1,
        city: 1,
        from: 1
      }
    )
      .skip(req.body.skippag)
      .limit(req.body.limit) //! Limite de pags
      .sort({ _id: -1 })
      .then(async (data) => {
        res.json({
          message: "Mostrando usuarios",
          status: true,
          data,
          pagination: {
            pag: req.params.pag,
            perpage: req.body.limit,
            pags: Math.ceil(cant / req.body.limit),
          },
        });
      })
      .catch((error) => {
        res.json({
          message: "Error al cargar todas publicaciones",
          status: false,
          error,
        });
      });
  } catch (err) {
    res.status(500).json(err);
  }
};

export const editPhoto = async (req, res) => {
  try {
    const { id } = req.params; //!El  id del usuario de quien vamos a editar la foto

    const userX = await User.findById({ _id: id }); //? Verifica si el usuario ya contiene una imagen

    let photoProfile = userX.photoProfile;

    if (req.files) {
      if (req.files.photoProfile) {
        photoProfile = await procesamientounidad(req.files, "photoProfile");
      }
    }

    const updld = User({
      photoProfile: photoProfile,
    });

    User.updateOne(
      { _id: id },
      {
        $set: {
          photoProfile,
        },
      }
    )

      .then((data) =>
        res.json({
          message: "Imagen cargada correctamente",
          status: true,
          updld,
          data,
        })
      )
      .catch((error) =>
        res.json({ message: "Error al carga", status: false, error })
      );
    return;
  } catch (error) {
    return res.json({
      message: "Error al intentar actualizar perfil, catch.....",
      status: false,
      error,
    });
  }
};
