import Cart from "../models/Cart";
import Post from "../models/Post";

export const getCart = async (req, res) => {
  const productsCart = await Cart.find();
  if (productsCart) {
    return res.status(200).json({
      message: "mostrando productos del carrito",
      status: true,
      productsCart,
    });
  } else {
    return res
      .status(404)
      .json({ message: "No hay productos  en el carrito", status: false });
  }
};

export const addProductCart = async (req, res) => {
  try {
    const { titulo, photos, price } = req.body;
    const enProduct = await Post.findOne({ titulo });
    const siVacio = titulo !== "" && photos !== "" && price !== "";
    const enCarrito = await Cart.findOne({ titulo });

    const { _id, name, photoProfile } = req.user;
    if (!enProduct) {
      return res.status(400).json({
        message: "este producto no se encuentra almacenado",
        status: false,
      });
    } else if (siVacio && !enCarrito) {
      const newProductInCart = new Cart({
        titulo,
        photos,
        price,
        amoun: 1,
        user: { _id, name, photoProfile },
      });

      await Post.findByIdAndUpdate(
        enProduct?._id,
        { inCart: true, titulo, photos, price },
        { new: true }
      )

        .then((data) =>
          res.json({
            message: "El producto fue agregado al carrito",
            status: true,
            newProductInCart,
            data,
          })
        )
        .catch((error) => console.error(error));
    } else if (enCarrito) {
      return res
        .status(400)
        .json({ message: "El producto ya esta en el carrito", status: true });
    }
  } catch (err) {
    res.status(500).json(err);
  }
};

export const editProductCart = async (req, res) => {
  try {
    const { productId } = req.params;
    const { query } = req.query;
    const body = req.body;

    const productSearch = await Cart.findById(productId);

    if (!query) {
      return res
        .status(404)
        .json({ message: "Sin enviar query", status: false });
    } else if (productSearch && query === "add") {
      body.amount = body.amount + 1;

      await Cart.findByIdAndUpdate(productId, body, {
        new: true,
      }).then((product) => {
        res.json({
          message: `El  producto: ${product.titulo} fue actualizado`,
          status: true,
          product,
        });
      });
    } else if (productSearch && query === "del") {
      body.amount = body.amount - 1;

      await Cart.findByIdAndUpdate(productId, body, {
        new: true,
      }).then((product) =>
        res.json({
          message: `El producto: ${product.titulo}  fue actualizado`,
          status: true,
          product,
        })
      );
    } else {
      return res
        .status(400)
        .json({ message: "Ha ocurrido error....", status: false });
    }
  } catch (err) {
    res.status(500).json(err);
  }
};

export const deleteProductCart = async (req, res) => {
  try {
    const { productId } = req.params;

    const productInCart = await Cart.findById(productId);

    const { titulo, photos, price, _id } = await Post.findOne({
      titulo: productInCart.titulo,
    });

    await Cart.findByIdAndDelete(productId);

    await Post.findByIdAndUpdate(
      _id,
      { inCart: false, titulo, photos, price },
      { new: true }
    )
      .then((product) =>
        res.json({
          message: `El  producto:  ${product.titulo}  fue  eliminado correctamente`,
          status: true,
        })
      )
      .catch((error) =>
        res.json({
          message: "Error al eliminar producto",
          status: false,
          error,
        })
      );
    return;
  } catch (err) {
    res.status(500).json(err);
  }
};
