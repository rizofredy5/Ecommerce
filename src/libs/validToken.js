import jwt from "jsonwebtoken";
import config from "../config";
import User from "../models/User";

export const validToken = async (req, res, next) => {
  try {
    const token = req.headers["valid-token"];

    if (!token) return res.status(403).json({ message: "no hay token" });

    const decoded = jwt.verify(token, config.SECRET);

    const user = await User.findById(decoded.id);
    if (!user) return res.status(404).json({ message: "no hay usuario" });
    req.user = user;
    next();
  } catch (err) {
    return res.status(500).json(err);
  }
};