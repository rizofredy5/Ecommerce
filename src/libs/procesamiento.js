const sharp = require("sharp");
const fs = require("fs");

export const procesamientounidad = async (files, fields) => {
  let imgsmbase64 = "";
  const element = files[fields][0];

  const imgsm = await sharp(element.path)
    .resize(1080, 1080)
    .toFile(element.destination + "/" + "sm" + element.originalname);
  //
  imgsmbase64 =
    "data:" +
    element.mimetype +
    ";base64," +
    fs.readFileSync(
      element.destination + "/" + "sm" + element.originalname,
      "base64"
    );

  fs.unlinkSync(element.path);
  fs.unlinkSync(element.destination + "/" + "sm" + element.originalname);

  return imgsmbase64;
};

export const procesamientoPublicacion = async(files) => {
  let photos = []
  for (let index = 0;  index < files.length; index ++) {
    const element = files[index]

    const imgmd = await sharp(element.path)
    .resize(1080, 1080)

    .toFile(element.destination+'/'+"md"+element.originalname)
    const imgmdbase64 = "data:"+element.mimetype+";base64," + fs.readFileSync(element.destination+"/"+"md"+element.originalname, 'base64')

    photos.push({
      "base64md": imgmdbase64
    })

    fs.unlinkSync(element.path)
    fs.unlinkSync(element.destination+"/"+"md"+element.originalname)
  }
  return photos
}
