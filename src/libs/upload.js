import multer from "multer";
import path from "path";

const storage =  multer.diskStorage({
    destination: path.join('storage'),
    filename: function(req,file,cb){
        const ext = file.originalname.split(".").pop();
        const filename  = `new-${Date.now()}.${ext}`;
        cb(null, filename)
    }
})

export  const  upload = multer({storage})